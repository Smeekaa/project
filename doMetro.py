import folium
import pandas


# read in metrolink data, add markers to map
def doMetro(map):
    metrolink_data = pandas.read_csv('data/stops.csv', error_bad_lines=False,
                                     low_memory=False)
    for line, name, lat, lon in zip(metrolink_data['line'], metrolink_data['stop_name'],
                                    metrolink_data['stop_lat'],
                                    metrolink_data['stop_lon']):
        details = '<strong>Line:</strong> ' + line + \
                  '<br> <strong>name:</strong> ' + name
        folium.Marker(location=[lat, lon], popup=details, icon=folium.Icon(color='blue', icon='subway', prefix='fa')) \
            .add_to(map)
    return map
