import requests
from bs4 import BeautifulSoup
import os
import time


# comment
def downloadfile(location):
    exists = os.path.isfile('data/' + location + '/listings.csv.gz')
    if exists:
        return
    else:
        try:
            time.sleep(1)
            url = requests.get('http://insideairbnb.com/get-the-data.html', stream=True)
            if url.status_code == 200:
                soup = BeautifulSoup(url.content, 'html.parser')
                tab = soup.find('table', {'class': 'table table-hover table-striped ' + location})
                data_link = tab.find('a', href=True)
                filename = data_link['href'].split("/")[-1]

                file_location = ("data/" + location + "/" + filename)

                os.makedirs(os.path.dirname(file_location), exist_ok=True)
                with open(file_location, "wb") as f:
                    r = requests.get(data_link['href'])
                    f.write(r.content)
                print(location + " added")
            else:
                print(url.status_code)
        except:
            print("error when downloading " + location)

 # comment
def downloadAll():
    places = ['amsterdam', 'antwerp', 'asheville', 'athens', 'barcelona', 'barossa-valley',
              'barwon-south-west-vic', 'beijing', 'berlin', 'bologna', 'bordeaux', 'boston', 'bristol',
              'broward-county', 'brussels', 'cambridge', 'cape-town', 'chicago', 'clark-county-nv', 'columbus',
              'copenhagen',
              'crete', 'denver', 'dublin', 'edinburgh', 'euskadi', 'florence', 'ghent', 'girona',
              'greater-manchester',
              'hawaii', 'hong-kong', 'istanbul', 'lisbon', 'london', 'los-angeles', 'lyon', 'madrid', 'malaga',
              'mallorca',
              'manchester',
              'melbourne', 'menorca', 'milan', 'montreal', 'naples', 'nashville', 'new-orleans', 'new-york-city',
              'oakland', 'oslo', 'ottawa', 'paris', 'portland', 'porto', 'prague', 'puglia',
              'rhode-island',
              'rio-de-janeiro', 'rome', 'san-diego', 'san-francisco', 'santa-clara-county',
              'santa-cruz-county',
              'seattle',
              'sevilla', 'sicily', 'stockholm', 'sydney', 'taipei', 'tasmania', 'thessaloniki', 'tokyo', 'toronto',
              'trentino',
              'twin-cities-msa',
              'valencia', 'vancouver', 'vaud', 'venice', 'victoria', 'vienna', 'washington-dc', 'western-australia']

    places_failed = []

    for place in places:
        exists = os.path.isfile('data/' + place + '/listings.csv.gz')
        if exists:
            continue
        else:
            try:
                time.sleep(1)
                url = requests.get('http://insideairbnb.com/get-the-data.html', stream=True)
                if url.status_code == 200:
                    soup = BeautifulSoup(url.content, 'html.parser')
                    tab = soup.find('table', {'class': 'table table-hover table-striped ' + place})
                    data_link = tab.find('a', href=True)
                    filename = data_link['href'].split("/")[-1]

                    file_location = ("data/" + place + "/" + filename)

                    os.makedirs(os.path.dirname(file_location), exist_ok=True)
                    with open(file_location, "wb") as f:
                        r = requests.get(data_link['href'])
                        f.write(r.content)
                    print(place + " added")
                else:
                    print(url.status_code)
            except:
                places_failed.append(place)
                print("error when downloading " + place)
    print("completed")
    return
