import folium
from folium.plugins import MarkerCluster
from rgb2hex import rgb2hex


# read in data, create a map based on average of coordinates, create markers for each entry, add details to markers,
# add to marker cluster, save the marker cluster to map
def doAirbnb(airbnb_data):
    try:
        # generates the map from dataset, using lat,long (with only markers on point, no extra details)
        map = folium.Map(location=[airbnb_data['latitude'].mean(), airbnb_data['longitude'].mean()],
                         # tiles="OpenStreetMap",
                         tiles="cartodbpositron",
                         zoom_start=12)

        cluster = MarkerCluster().add_to(map)

        # assigning data
        for lat, long, name, summary, price, accomNum, bedrooms, \
            beds, roomType, propertyType, \
            bathrooms, listingURL, listingPic in zip(airbnb_data['latitude'], airbnb_data['longitude'],
                                                     airbnb_data['name'], airbnb_data['summary'],
                                                     airbnb_data['price'], airbnb_data['accommodates'],
                                                     airbnb_data['bathrooms'], airbnb_data['bedrooms'],
                                                     airbnb_data['beds'], airbnb_data['room_type'],
                                                     airbnb_data['bathrooms'], airbnb_data['listing_url'],
                                                     airbnb_data['picture_url']):
            dollartoGBP = price
            stringPrice = str(dollartoGBP)
            stringName = str(name)
            stringSummary = str(summary)
            stringAccommodates = str(accomNum)
            stringBedrooms = str(bedrooms)
            stringBeds = str(beds)
            stringRoomType = str(propertyType)
            stringBathrooms = str(bathrooms)
            url = '<a href="' + listingURL + '">visit listing</a>'

            # creating the popup details for the marker
            details = '<strong>Title: </strong>' + stringName + \
                      '<br> <strong>Summary: </strong > ' + stringSummary + \
                      '<br> <strong>Price: </strong> ' + "£" + stringPrice + \
                      '<br> <strong>Accommodates: </strong> ' + stringAccommodates + \
                      '<br> <strong>Bedrooms: </strong> ' + stringBedrooms + \
                      '<br> <strong>Beds: </strong> ' + stringBeds + \
                      '<br> <strong>Place Type: </strong> ' + stringRoomType + \
                      '<br> <strong>Bathrooms: </strong> ' + stringBathrooms + \
                      '<br> <strong>url:</strong> ' + str(url)

            # creating the markers, colour based on price
            color_hex = rgb2hex(price, airbnb_data['price'].min(), airbnb_data['price'].max())
            folium.RegularPolygonMarker(location=[lat, long], fill_color=color_hex, number_of_sides=30,
                                        popup=details, weight=3, fill_opacity=3, radius=12).add_to(cluster)

        return map
    except:
        print("failed")
        return
