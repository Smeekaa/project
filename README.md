# Eben Bentley - Dissertation Project

An application derived from Open Data

Postcode search to retrive all crimes for that postcode, with a variable distance

Airbnb date for 90 cities, with filtering on price and number of guests.
Airbnb Manchester / Greater-Manchester features metrolink stops and flood likelihood 

UK Crime Data searching - visualise crime occurances in the UK from a postcode
## requirements
```python
python 3.6+ 
```

## Usage

```python
pip install -r requirements.txt
or 
py -m install -r requirements.txt
python project.py
```

