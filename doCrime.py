import pandas
from haversine import haversine
import folium
from folium.plugins import MarkerCluster

places = []


# used for reading in data based on user input and to display what data date they requested
# as data is separated by filename for the month it was collected
def doName(time):
    global date
    if time == 1:
        date = '2018-12'
        places.append(date)
        print("December 2018")
    elif time == 2:
        date = '2018-11'
        places.append(date)
        print("November 2018")
    elif time == 3:
        date = '2018-10'
        places.append(date)
        print("October 2018")
    elif time == 4:
        date = '2018-09'
        places.append(date)
        print("September 2018")
    elif time == 5:
        date = '2018-08'
        places.append(date)
        print("August 2018")
    elif time == 6:
        date = '2018-07'
        places.append(date)
        print("July 2018")
    elif time == 7:
        date = '2018-06'
        places.append(date)
        print("June 2018")
    elif time == 8:
        date = '2018-05'
        places.append(date)
        print("May 2018")
    elif time == 9:
        date = '2018-04'
        places.append(date)
        print("April 2018")
    elif time == 10:
        date = '2018-03'
        places.append('2018-03')
        places.append(date)
        print("March 2018")
    elif time == 11:
        date = '2018-02'
        places.append(date)
        print("February 2018")
    elif time == 12:
        date = '2018-01'
        places.append(date)
        print("January 2018")

    return date


# list of iterable names of crime data
names = ['2019-01-avon-and-somerset-street.csv', '2019-01-bedfordshire-street.csv', '2019-01-cambridgeshire-street.csv',
         '2019-01-cheshire-street.csv', '2019-01-city-of-london-street.csv', '2019-01-cleveland-street.csv',
         '2019-01-cumbria-street.csv', '2019-01-derbyshire-street.csv', '2019-01-devon-and-cornwall-street.csv',
         '2019-01-dorset-street.csv', '2019-01-durham-street.csv', '2019-01-dyfed-powys-street.csv',
         '2019-01-essex-street.csv',
         '2019-01-gloucestershire-street.csv', '2019-01-greater-manchester-street.csv', '2019-01-gwent-street.csv',
         '2019-01-hampshire-street.csv', '2019-01-hertfordshire-street.csv', '2019-01-humberside-street.csv',
         '2019-01-lancashire-street.csv', '2019-01-leicestershire-street.csv', '2019-01-lincolnshire-street.csv',
         '2019-01-merseyside-street.csv', '2019-01-metropolitan-street.csv', '2019-01-norfolk-street.csv',
         '2019-01-north-wales-street.csv',
         '2019-01-north-yorkshire-street.csv', '2019-01-northamptonshire-street.csv',
         '2019-01-northern-ireland-street.csv', '2019-01-northumbria-street.csv',
         '2019-01-nottinghamshire-street.csv', '2019-01-south-wales-street.csv', '2019-01-south-yorkshire-street.csv',
         '2019-01-staffordshire-street.csv', '2019-01-suffolk-street.csv',
         '2019-01-surrey-street.csv', '2019-01-sussex-street.csv', '2019-01-thames-valley-street.csv',
         '2019-01-warwickshire-street.csv', '2019-01-west-mercia-street.csv', '2019-01-west-midlands-street.csv',
         '2019-01-west-yorkshire-street.csv', '2019-01-wiltshire-street.csv']


# latitude, longitude, render distance and a time-period used to generate a crime data map
# average for each dataset is computed, and compared to the user entered coordinates, return the dataset that has
# the closest average to the user coordinates, hence will be the police organisation that cover that area
def doCrime(lat, lon, render_dist, time):
    time = int(time)
    global colour
    render_dist = int(render_dist)

    # dictionary to save the name of file and distance
    distances = {}

    # determine which time period data to use
    date = doName(time)

    # assign the filepath
    filepath = 'data/police/' + date + '/'

    render_dist = int(render_dist)
    user = (float(lat), float(lon))

    # loop over data, replace filename with user supplied date

    for name in names:
        # january 2018 does not contain this set of data
        if name == '2019-01-avon-and-somerset-street.csv' and time == 12:
            continue
        name = name.replace(name[:7], date)

        # read in data, compute average of the total coordinates, add to dictionary and return the data which is
        # closest to the user input coordinates
        crime_data = pandas.read_csv(filepath + name, error_bad_lines=False, low_memory=False)
        police_avg = (crime_data['Latitude'].mean(), crime_data['Longitude'].mean())
        distance = haversine(user, police_avg, unit='mi')
        distances[name] = distance

    # return the name of data that is cloest to the user coordinates
    place = min(distances, key=(lambda k: distances[k]))

    # read in closest police data
    crime_data = pandas.read_csv(filepath + place, error_bad_lines=False, low_memory=False)

    # generate a map based on the user input coordinates
    map = folium.Map(location=[float(lat), float(lon)],
                     # tiles="OpenStreetMap",
                     tiles="cartodbpositron",
                     zoom_start=12)

    # popup details to 'home' marker
    details = '<strong>Latitude: </strong>' + str(lat) + \
              '<br> <strong>Longitude: </strong>' + str(lon)

    # create the home marker (user entered location)
    folium.Marker(location=[float(lat), float(lon)], icon=folium.Icon(color='blue', icon='home', prefix='fa'),
                  popup=details).add_to(
        map),



    # display key for the type of markers and their type of crime
    marker_legend = '''
                    <div style="position: fixed; 
                                bottom: 450px; left: 25px; width: 100px; height: 180px; 
                                z-index:9999; font-size:14px;
                                ">&nbsp; Drugs &nbsp; <br> <i class="fa fa-map-marker fa-2x" style="color:purple"></i><br>
                                  &nbsp; Theft/Burglary/Robbery/Shoplifting &nbsp;<i class="fa fa-map-marker fa-2x" style="color:black"></i><br><br>
                                  &nbsp; Violence and sexual offences &nbsp;<i class="fa fa-map-marker fa-2x" style="color:green"></i><br><br>
                                  &nbsp; Criminal damage and arson &nbsp; <br><i class="fa fa-map-marker fa-2x" style="color:red"></i><br><br>
                                  &nbsp; Vehicle crime &nbsp; <i class="fa fa-map-marker fa-2x" style="color:purple"></i><br><br>
                                  &nbsp; Anti-social behaviour &nbsp; <br><i class="fa fa-map-marker fa-2x" style="color:orange"></i><br><br>
                                  &nbsp; Public order &nbsp; <i class="fa fa-map-marker fa-2x" style="color:orange"></i><br><br>
                                  &nbsp; Other crime &nbsp; <i class="fa fa-map-marker fa-2x" style="color:gray"></i>
                    </div>
                    '''
    map.get_root().html.add_child(folium.Element(marker_legend))

    cluster = MarkerCluster().add_to(map)

    # loop over data rows, assign respectfully
    try:
        for lon, lat, type, outcome, in zip(crime_data['Latitude'], crime_data['Longitude'],
                                            crime_data['Crime type'], crime_data['Last outcome category']):

            place = lon, lat
            dist = haversine(user, place, unit='mi')

            # test if the crime location is within the range of the user input location
            # if so, assign the colour for type of crime
            if dist <= render_dist:
                details = '<strong>type: </strong> ' + str(type) + \
                          '<br> <strong>outcome: </strong> ' + str(outcome)

                if type == 'Drugs':
                    colour = 'purple'

                elif type == 'Burglary':
                    colour = 'black'
                elif type == 'Bicycle theft':
                    colour = 'black'
                elif type == 'Robbery':
                    colour = 'black'
                elif type == 'Shoplifting':
                    colour = 'black'
                elif type == 'Other theft':
                    colour = 'black'

                elif type == 'Violence and sexual offences':
                    colour = 'green'
                elif type == 'Criminal damage and arson':
                    colour = 'red'

                elif type == 'Vehicle crime':
                    colour = 'purple'

                elif type == 'Anti-social behaviour':
                    colour = 'orange'
                elif type == 'Public order':
                    colour = 'orange'

                elif type == 'Other crime':
                    colour = 'gray'

                # add the crime marker to the map
                folium.Marker(location=[lon, lat], icon=folium.Icon(color=colour, icon='exclamation', prefix='fa'),
                              popup=details).add_to(cluster)

    except:

        print("failed")

    return map
