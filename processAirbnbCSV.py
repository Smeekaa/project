import pandas


# read in the data, remove $ symbols from price data, remove symbols
# check the number to accommodate does not exceed highest entry in data
def processCSV(min, max, accom, location):
    airbnb_data = pandas.read_csv('data/' + location + '/' + 'listings.csv.gz', compression='gzip',
                                  error_bad_lines=False, low_memory=False)
    if int(accom) > airbnb_data['accommodates'].max():
        return print("no properties available")
    # replace specific character from dataset (dollar signs for prices.. removed and converted to GBP
    # removed a name that caused crashes when process
    airbnb_data['price'] = airbnb_data['price'].astype(str)
    airbnb_data['price'] = airbnb_data['price'].str.replace('$', '')
    airbnb_data['price'] = airbnb_data['price'].str.replace(',', '')
    airbnb_data['price'] = airbnb_data['price'].astype(float)
    airbnb_data['name'] = airbnb_data['name'].str.replace('{{RED BĀ SERVICED ACCOMMODATION}}', '')

    # clean dataset to have prices between a range
    # filter dataset to show for input number of guests to accommodate
    # greatly improves processing speed and compilation time when filtered
    airbnb_data = airbnb_data[(airbnb_data.price < int(max)) & (airbnb_data.price > int(min))]
    airbnb_data = airbnb_data[(airbnb_data.accommodates == int(accom))]

    return airbnb_data
