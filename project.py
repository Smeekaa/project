from downloadAirbnb import downloadfile, downloadAll
from processAirbnbCSV import processCSV
from doAirbnb import doAirbnb
from doMetro import doMetro
from doFlooding import doFlooding
from doCrime import doCrime
import webbrowser
import time
import json
import requests
import geocoder

# all airbnb data locations
places = ['amsterdam', 'antwerp', 'asheville', 'athens', 'barcelona', 'barossa-valley',
          'barwon-south-west-vic', 'beijing', 'berlin', 'bologna', 'bordeaux', 'boston', 'bristol',
          'broward-county', 'brussels', 'cambridge', 'cape-town', 'chicago', 'clark-county-nv', 'columbus',
          'copenhagen',
          'crete', 'denver', 'dublin', 'edinburgh', 'euskadi', 'florence', 'ghent', 'girona',
          'greater-manchester',
          'hawaii', 'hong-kong', 'istanbul', 'lisbon', 'london', 'los-angeles', 'lyon', 'madrid', 'malaga',
          'mallorca',
          'manchester',
          'melbourne', 'menorca', 'milan', 'montreal', 'naples', 'nashville', 'new-orleans', 'new-york-city',
          'oakland', 'oslo', 'ottawa', 'paris', 'portland', 'porto', 'prague', 'puglia',
          'rhode-island',
          'rio-de-janeiro', 'rome', 'san-diego', 'san-francisco', 'santa-clara-county',
          'santa-cruz-county',
          'seattle',
          'sevilla', 'sicily', 'stockholm', 'sydney', 'taipei', 'tasmania', 'thessaloniki', 'tokyo', 'toronto',
          'trentino',
          'twin-cities-msa',
          'valencia', 'vancouver', 'vaud', 'venice', 'victoria', 'vienna', 'washington-dc', 'western-australia']

# output for the user to view airbnb locations
places_string = ("locations:\namsterdam, antwerp, asheville, athens, barcelona, barossa-valley,\n"
                 "barwon-south-west-vic, beijing, berlin, bologna, bordeaux, boston, bristol,\n"
                 "broward-county, brussels, cambridge, cape-town, chicago, clark-county-nv, columbus, copenhagen,\n"
                 "crete, denver, dublin, edinburgh, euskadi, florence, ghent, girona, greater-manchester, "
                 "hawaii,\nhong-kong, istanbul, lisbon, london, los-angeles, lyon, madrid, malaga, mallorca, "
                 "manchester,\n"
                 "melbourne, menorca, milan, montreal, naples, nashville, new-orleans, new-york-city,\n"
                 "oakland, oslo, ottawa, paris, portland, porto, prague, puglia, "
                 "rhode-island,\n"
                 "rio-de-janeiro, rome, san-diego, san-francisco, santa-clara-county, santa-cruz-county,"
                 "seattle,\n"
                 "sevilla, sicily, stockholm, sydney, taipei, tasmania, thessaloniki, tokyo, toronto, trentino, "
                 "twin-cities-msa,\n"
                 "valencia, vancouver, vaud, venice, victoria, vienna, washington-dc, western-australia\n")

print("'help' to view commands")

# application runs until user closes
while True:

    # read in user input
    user_input = input("enter a command: ")

    # exit application
    if user_input == 'exit':
        exit(0)

    # handle crime, request details from user (location, distance, time period), verify input, if valid,
    # create map with respected input data
    # api used to get coordinates from postcode
    if user_input == 'crime':
        location = input("location (postcode or me): ")
        distance = input("distance (miles): ")
        if distance.isdigit():
            time_period = input("time period 1-12 (December 2018 - January 2018): ")
            if time_period.isdigit():
                if location == 'me':
                    me = geocoder.ip('me')
                    me = me.latlng
                    lat = me[0]
                    lon = me[1]
                    map = doCrime(lat, lon, distance, time_period)
                    map.save(outfile='map.html')
                    time.sleep(1)
                    webbrowser.open('map.html')
                else:
                    response = requests.get('http://api.getthedata.com/postcode/' + location)
                    if response.status_code == 200:
                        try:
                            data = json.loads(response.text)
                            data = data['data']
                            lat = (data['latitude'])
                            lon = (data['longitude'])
                            map = doCrime(lat, lon, distance, time_period)
                            map.save(outfile='map.html')
                            time.sleep(1)
                            webbrowser.open('map.html')
                            while True:
                                new_time = input("enter a new time period: ")
                                if new_time == "exit":
                                    break
                                map = doCrime(lat, lon, distance, new_time)
                                map.save(outfile='map.html')
                                time.sleep(1)
                                webbrowser.open('map.html')
                        except:
                            print("failed with postcode data")
                    else:
                        print("bad response from postcode api")
            else:
                print("invalid time period")
        else:
            print("invalid distance")

    # help section for the user
    elif user_input == 'help':
        print('airbnb - returns a list of airbnb locations, any of listed places are a command')
        print('crime - display crimes based on postcode')
        print('all-airbnb - download all airbnb locations')


    # download all the available airbnb dacrtasets (improves runtime speed)
    elif user_input == 'all-airbnb':
        downloadAll()

    # handle airbnb, request input from the user (location, min/max price, number of guests), if dataset not downloaded,
    # download the data, and run from local file
    # if user enters manchester or greater-manchester, option to overlay more data is present
    elif user_input == 'airbnb':
        print(places_string)

        location = input("enter a location: ")

        if location in places:
            minPrice = input("min price: ")
            if minPrice.isdigit():
                maxPrice = input("max price: ")
                if maxPrice.isdigit():
                    guestsNum = input("number of guests: ")
                    if guestsNum.isdigit():
                        # download particular location
                        try:
                            downloadfile(location)
                        except:
                            print("error finding file")

                        # process the csv
                        try:
                            airbnb_data = processCSV(minPrice, maxPrice, guestsNum, location)
                            # map the markers
                            # ask user to overlay other manchester data
                            map = doAirbnb(airbnb_data)

                            if location == 'manchester' or location == 'greater-manchester':
                                overlay = input("metrolink stops (y/n) ")

                                if overlay == "y" or overlay == 'n':
                                    if overlay == 'y':
                                        map = doMetro(map)
                                else:
                                    continue

                                overlay = input("flood data (y/n) ")
                                if overlay == "y" or overlay == 'n':
                                    if overlay == 'y':
                                        map = doFlooding(map)
                                else:
                                    continue

                            # save map and display
                            map.save(outfile='map.html')
                            time.sleep(1)
                            webbrowser.open('map.html')

                        except:
                            print("error filtering data")
                    else:
                        print("invalid number of guests")
                else:
                    print("invalid maximum price")
            else:
                print("invalid minimum price")
        else:
            print("invalid location")
