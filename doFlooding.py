import folium
import pandas


# read in flooding data, if postcode is within manchester, add to map
def doFlooding(map):
    flood_data = pandas.read_csv('data/floods.csv', error_bad_lines=False,
                                 low_memory=False)
    for postcode, risk, date, lat, lon in zip(flood_data['postcode'], flood_data['risk'],
                                              flood_data['date'],
                                              flood_data['lat'], flood_data['lon']):

        details = '<br> <strong>Postcode:</strong> ' + postcode + \
                  '<br> <strong>date:</strong> ' + date + \
                  '<br> <strong>risk:</strong> ' + risk
        if risk == "None":
            continue
        if postcode[:2] == 'MK':
            continue
        elif postcode[:1] == 'M':
            if risk == 'Low':
                folium.RegularPolygonMarker(location=[lat, lon], fill_color='#00FF00', number_of_sides=3, radius=5,
                                            popup=details).add_to(map)
            if risk == 'Medium':
                folium.RegularPolygonMarker(location=[lat, lon], fill_color='#FF8C00', number_of_sides=3, radius=5,
                                            popup=details).add_to(map)
            elif risk == 'High':
                folium.RegularPolygonMarker(location=[lat, lon], fill_color='#ff0000', number_of_sides=3, radius=5,
                                            popup=details).add_to(map)
        else:
            continue
    return map
